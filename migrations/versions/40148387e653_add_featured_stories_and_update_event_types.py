"""womensactivism_v3

Revision ID: f32a9ba548dc
Revises: 51f6f82e2af8
Create Date: 2018-03-05 22:31:02.689146

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'f32a9ba548dc'
down_revision = '51f6f82e2af8'
branch_labels = None
depends_on = None

old_options = ('story_created', 'user_created', 'story_edited', 'story_deleted', 'comment_edited', 'comment_deleted',
               'feature_story_edited')
new_options = old_options + (
    'then_and_now_edited', 'story_flagged', 'user_edited', 'login_failed', 'login_success', 'featured_story_added',
    'featured_story_edited', 'featured_story_hidden')

old_type = sa.Enum(*old_options, name='event_type')
new_type = sa.Enum(*new_options, name='event_type')
tmp_type = sa.Enum(*new_options, name='_type')


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('featured_stories',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('story_id', sa.Integer(), nullable=False),
                    sa.Column('left_right', sa.Boolean(), nullable=False),
                    sa.Column('is_visible', sa.Boolean(), nullable=False),
                    sa.Column('quote', sa.Text(), nullable=False),
                    sa.Column('rank', sa.Integer(), nullable=True),
                    sa.ForeignKeyConstraint(['story_id'], ['stories.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )

    op.add_column('users', sa.Column('password_hash', sa.String(length=128), nullable=True))
    op.add_column('users', sa.Column('subscription', sa.Boolean(), nullable=True))

    # Create a temporary "_type" type, convert and drop the "old" type
    tmp_type.create(op.get_bind(), checkfirst=False)
    op.execute('ALTER TABLE events ALTER COLUMN type TYPE _type'
               ' USING type::TEXT::_type')
    old_type.drop(op.get_bind(), checkfirst=False)
    # Create and convert to the "new" type type
    new_type.create(op.get_bind(), checkfirst=False)
    op.execute('ALTER TABLE events ALTER COLUMN type TYPE event_type'
               ' USING type::TEXT::event_type')
    tmp_type.drop(op.get_bind(), checkfirst=False)

    op.drop_constraint('events_user_guid_fkey', 'events', type_='foreignkey')
    # op.add_column('featuredstories', sa.Column('rank', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('featured_stories')

    op.drop_column('users', 'password_hash')
    op.drop_column('users', 'subscription')

    # op.execute('UPDATE responses SET type = \'note\' WHERE type IN \(\'letters\', \'envelopes\'\)')
    # Create a temporary "_type" type, convert and drop the "new" type
    tmp_type.create(op.get_bind(), checkfirst=False)
    op.execute('ALTER TABLE events ALTER COLUMN type TYPE _type'
               ' USING type::TEXT::_type')
    new_type.drop(op.get_bind(), checkfirst=False)
    # Create and convert to the "old" type type
    old_type.create(op.get_bind(), checkfirst=False)
    op.execute('ALTER TABLE events ALTER COLUMN type TYPE event_type'
               ' USING type::TEXT::type')
    tmp_type.drop(op.get_bind(), checkfirst=False)

    op.create_foreign_key('events_user_guid_fkey', 'events', 'users', ['user_guid'], ['guid'])
    # ### end Alembic commands ###
